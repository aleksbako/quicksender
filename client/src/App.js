import React from 'react'

import './App.css';
import {BrowserRouter as Router,Routes, Route} from "react-router-dom"
import ChatRoom from './Components/PostLogin/Chat/ChatRoom';
import LoginPage from './Components/Login/LoginPage'
import Main from './Components/Home/Main';
import PostMain from './Components/PostLogin/Home/Main';
import Search from './Components/PostLogin/Search/Search';
import Account from './Components/PostLogin/Account/Account';
import Profile from './Components/PostLogin/Profile/Profile';
import Register from "./Components/Register/Register"

function App(){
 
   

    return (
     <Router>
       <Routes>
         <Route path={"/"} element={<Main/>}/>
         <Route path={"/post"} element={<PostMain/>}/>
         <Route path={"/search"} element={<Search />}/>
         <Route path={"/acc"} element={<Account/>}/>
         <Route path={"/login"} exact element={<LoginPage />}/>
         <Route path={"/chat/:id"} element={<ChatRoom />}/>
         <Route path={"/profile/:id"} element={<Profile />}/>
         <Route path={"/register"} element={<Register />}/>
       </Routes>
     </Router>
    )
  
}

export default App;
