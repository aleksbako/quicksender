import React, {Component} from "react"
import {Container, Row, Col, Table, Image, ListGroup} from "react-bootstrap"
import TopNav from "../TopNav"
import userimg from "./img/UserImage.jpg"
import withRouter from "../../../withRouter"
import Result from "./Result"
import "./custom.css"

class Search extends Component{
    constructor(props){
        super(props)
       
        this.state = {
            searchList: []
        }
    }
   
  async  componentDidMount(){
        
        document.body.style.color = "white";
        document.body.style.background= "#1a1817"
    
       let searchedVal = this.props.router.location.state.search
      console.log(searchedVal)

      const token = document.cookie.split("=")[1]
        const response = await fetch("http://localhost:8080/restricted/search", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
            method: "POST",
            mode:"cors",
            body: new URLSearchParams("search="+searchedVal)
          }).then(function(response) {
             
            return response.json();
          }).then(function(data) {
            return data; // this will be a string
          });

          this.setState(()=>{
              return {searchList : response.contacts}
          })
    }
    render(){
        const List = this.state.searchList.map(data => {
            return <Result name={data} />
        })
        return(<Container fluid >
            <TopNav/>
            <Row>
                <h1>List of searched results</h1>
            </Row>
            <Row>
                <Col >
                <Container >
<ListGroup >    
 {List}
 </ListGroup>  
</Container>
</Col>
            </Row>

        </Container>)
    }
}
export default withRouter(Search)