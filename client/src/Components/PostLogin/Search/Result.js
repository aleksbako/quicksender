import React from "react"
import {Image,Button, ListGroup, Col , Row, Card} from "react-bootstrap"
import {Link} from "react-router-dom"
import userimg from "./img/UserImage.jpg"
function Result(props){
    return (
<Link className="mt-2" to={{pathname: "/profile/"+props.name, username: props.name} }>

<ListGroup.Item className="cardcolor border">

      <Card className="cardcolor text-light">
      <Row md={2}>
      <Col>
      <Image src={userimg} rounded className="imagez" />
      </Col>
      <Col >
      <p>{props.name}</p>
      </Col>
      </Row>
      
    
      </Card>
   
   
    </ListGroup.Item>  </Link>
   
    )

}export default Result