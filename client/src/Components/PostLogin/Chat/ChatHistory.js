import React, { Component } from "react";
import {Row} from "react-bootstrap"
import Message from "./Message"
import "./chat.css"

class ChatHistory extends Component {
  constructor(props){
    super(props)
    this.state = {
      username : ""
    }
  }

 async componentDidMount(){
    const token = document.cookie.split("=")[1]
    const response = await fetch("http://localhost:8080/restricted/usrn", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then(function(response) {
       
      return response.text();
    }).then(function(data) {
      
      return data
    });
    
   this.setState(() => {return {username : response}})
  }
  render() {
    const messages = this.props.chatHistory.map((msg, index) => {
     let data = msg.data.split(" : ")
      if(this.state.username == data[0].trim()){
        
     return <Message key={index} username={data[0].trim()} message={data.slice(1,data.length).toString()} offset={0} color={"bg-light"}/>
    }else{
     
      return <Message key={index} username={data[0]} message={data.slice(1,data.length).toString()} offset={8} color={"bg-primary"}/>
    }
     
    });

    return (
      <Row className="border text-dark chatBox">
      
        {messages}
    
      </Row>
    );
  }
}

export default ChatHistory;