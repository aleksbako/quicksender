import React from "react"
import {Card, Col, Row, Image} from "react-bootstrap"
import {NavLink} from "react-router-dom"
import userimg from "./img/UserImage.jpg"
function Recent(props){
    return <Card className="bg-secondary "> 
     
    <Card.Body>
      <Row md={3}>
    <Col>
  <Image className="imagez" src={userimg} />
  </Col>
  <Col>
  <Row>
  <NavLink to={"/chat/"+props.id}><Card.Header>{props.username}</Card.Header>
  </NavLink>
  </Row>
    <Row>
      <Card.Text>
        *display last message*
      </Card.Text>
      </Row>
      </Col>
      </Row>
    </Card.Body>
    
  </Card> 
}
export default Recent