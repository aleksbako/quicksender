import React,{ Component } from "react";
import Recent from "./Recent";

class RecentList extends Component{
  constructor(props){
    super(props)
    this.state = {
        results : []
    }
  }
    async componentDidMount(){
        const token = document.cookie.split("=")[1]
    const response = await fetch("http://localhost:8080/restricted/chatcontact", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then(function(response) {
       
      return response.json();
    }).then(function(data) {
      
      return data
    });
   
    this.setState(() => {return {results: response.Contacts}})
    }
    
   render(){
     const List = this.state.results.map((data,index) => {
       return <Recent key={index} username={data.contact_name} id={data.chat_id}/>
     })
       return (<div>{List}</div>)
   }
}
export default RecentList