import React, { Component } from "react";
import {Form} from "react-bootstrap"


class ChatInput extends Component {
  render() {
    return (
      <Form className="ChatInput">
     
     
      <Form.Control type="text" placeholder="Enter message" onKeyDown={this.props.send}/>
    
    
    </Form>
    
    );
  }
}

export default ChatInput;