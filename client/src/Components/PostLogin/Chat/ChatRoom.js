import { Component, useParams } from "react";
import {Row, Col, Container, Button, ListGroup, Card, CardGroup, CardImg, Image} from "react-bootstrap"
import {BsArrowRightCircleFill} from "react-icons/bs"
import { sendMsg,connect, newsocket } from '../../../api';
import Header from './Header'
import ChatHistory from './ChatHistory'
import ChatInput from './ChatInput'
import "./chat.css"
import CardHeader from "react-bootstrap/esm/CardHeader";

import RecentList from "./RecentList";
import TopNav from "../TopNav";

class ChatRoom extends Component{
constructor(props){
    super(props)
    this.state = {
      chatHistory: []
    }
   
  }
  componentDidMount(){
  
    document.body.style.color = "white";
    document.body.style.background= "#1a1817"
      var url = window.location.pathname;
      var id = url.substring(url.lastIndexOf('/') + 1);
  
    newsocket(id) 
    connect((msg) => {
      
      
     
      this.setState(() => ({
        
        chatHistory: [...this.state.chatHistory, msg]
      }))
    
     
    });
  
  }
  send(event){
    
    if(event.keyCode == 13) {
     let a =  JSON.stringify({
        from: document.cookie.split("=")[1],
        data: event.target.value
      });
      sendMsg(a);
      event.target.value = "";
      event.preventDefault()
    }
  }

  render(){
    return (
      <Container fluid >
        <TopNav />
      <Container className=" mt-5">
        <CardGroup >
      <Card>
        <CardHeader ></CardHeader>
      <Row md={2}>
          <Col >
          <Card.Header className="bg-dark">
          <h5 className="mb-0 py-1">Recent Conversations</h5>
          </Card.Header>
          <Card className="oldMsg_Cont">


     <RecentList />

</Card>
        </Col>
        <Col>
       
        <Card>
        <ChatHistory chatHistory={this.state.chatHistory} />
        
        <Row md={2}>
          <Col>
        <ChatInput send={this.send} />
        </Col>
        <Col>
        <Button onClick={this.send}> {<BsArrowRightCircleFill/>}</Button>
        </Col>
        
        </Row>
        </Card>
     
        </Col>

        </Row>
        </Card>
        </CardGroup>
      </Container>
      </Container>
    )
  }
}
export default ChatRoom