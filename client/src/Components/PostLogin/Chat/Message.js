import React from "react"
import { Card, Col, Row, Image } from "react-bootstrap"
import userimg from "./img/UserImage.jpg"
function Message(props){
    return( <Row className="mt-3" md={1}><Col md={{span: 4,offset: props.offset}}><Card className={props.color} >
        
        <Row>
        <Col md={{span: 1}}>
          <Image className="chatImage mt-2 " src={userimg} />
          </Col>
          <Col md={{offset: 1}}>
        
            
           
          <Card.Body >
           
            <Row>
            <Card.Text >
                {props.message}
            </Card.Text>
            </Row>
          </Card.Body>
          </Col>
          </Row>
    </Card> </Col> </Row>)
    
}export default Message