import React from "react"
import {Button,Col,Row, Image, Card} from "react-bootstrap"
import userimg from "./img/UserImage.jpg"

function Contact(props){
    return <Col><Button className="cardcolor border mt-3" onClick={() => props.chat(props.chat_id)}> <Card><Image className="imagez" src={userimg}/> </Card>{props.username} </Button></Col>
}
export default Contact