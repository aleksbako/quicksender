import React from "react"
import {Row} from "react-bootstrap"
import Contact from "./Contact"
function Contacts(props){
const List = props.List.map(data => {
    return (<Contact key={data.username} username={data.username} chat_id={data.id} chat={props.chat}/>)
})
return <Row  md={4}>{List}</Row>
}
export default Contacts