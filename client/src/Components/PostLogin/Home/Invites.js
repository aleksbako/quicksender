import React from "react"
import Invite from "./Invite"
function Invites(props){
    const List = props.List.map(data => {
        return (<Invite key={data.username} username={data.username} Accept={props.Accept} Decline={props.Decline}/>)
    })
    return <>{List}</>
    }
    export default Invites