import React from "react"
import userimg from "./img/UserImage.jpg"
import { useNavigate } from "react-router"
import {Link} from "react-router-dom"
import {Button,Row, Col, Image,Card} from "react-bootstrap"

function Invite(props){
    const navigate = useNavigate()


    return <Row md={3} className="mt-3" >
        <Col>
        <Link to={{pathname: "/profile/"+props.username, username: props.username}}> <Button className="cardcolor border"> <Card><Image className="imagez" src={userimg}/> </Card>{props.username} </Button></Link>
        
        </Col>
      
    
        <Col ><Button onClick={() => props.Accept(props.username)}> Accept </Button></Col>
        <Col ><Button onClick={() => props.Decline(props.username)}> Decline </Button></Col>
        
        
        </Row>
}
export default Invite