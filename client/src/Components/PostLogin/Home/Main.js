import React, {Component} from "react"
import {Navigate} from "react-router-dom"
import {Container, Row, Col,Button,Form,Card} from "react-bootstrap"
import TopNav from "../TopNav"
import Contacts from "./Contacts"
import Invites from "./Invites"
import withRouter from "../../../withRouter"
import { newsocket } from "../../../api"
import "./post.css"
class Main extends Component{
    constructor(props){
        super(props)
        this.state = {
            contacts: [],
            invites : []

        }
        this.accept = this.accept.bind(this)
        this.decline = this.decline.bind(this)
        this.ChatRoom = this.ChatRoom.bind(this)
    }
    async componentDidMount(){
        document.body.style.color = "white";
        document.body.style.background= "#1a1817"

        const token = document.cookie.split("=")[1]
        const response = await fetch("http://localhost:8080/restricted/post", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }).then(function(response) {
             
            return response.json();
          }).then(function(data) {
            
            return data
          });
          console.log(response)
         
         this.setState(() => {return {contacts : response.Contacts, invites : response.invites}}); 
        
    }
    async accept(username) {
        const token = document.cookie.split("=")[1]
        const response = await fetch("http://localhost:8080/restricted/addContact", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
            method: "POST",
            mode:"cors",
            body: new URLSearchParams("username="+username)
          });
          this.forceUpdate()
    }

    async decline(username) {
        const token = document.cookie.split("=")[1]
        const response = await fetch("http://localhost:8080/restricted/uninv", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
            method: "POST",
            mode:"cors",
            body: new URLSearchParams("username="+username)
          })
          this.forceUpdate()
    }


    ChatRoom(chat_id){
        this.props.router.navigate("/chat/"+chat_id)
       
    }

    
    render(){
        if(document.cookie == ""){
          
            return <Navigate to={"/"} />
        }
        const invites =  this.state.invites.map(name => {
            return {username: name}
        })
     
        const contacts =  this.state.contacts.map(data => {
            return {username: data.contact_name, id: data.chat_id}
        })
        
     
     
        return (<Container fluid>
            <TopNav />
            
            <Row md={2}>
                <Col >
                <Card className="invitecard">
                <Container>
                    
                    <Row md={3}>
                        <Col>
                        <Row>
                        <Form.Control size="md" type="text" placeholder="Large text" />
                
                        <Button variant="secondary">Find friend</Button>
                       
                        </Row>
                        <Row>
            <h3>Invites</h3>
            </Row>
            <Row md={1} >
                <Invites Accept={this.accept} Decline={this.decline} List={invites} />
                </Row>
         
            </Col>
           
    
            </Row>
         
            </Container>
            </Card>
            </Col>

            <Col>
            <Card className="contactcard">
            <Container>
                <Row>
                <h3>List of contacts</h3>
                </Row>
                
                <Contacts List={contacts} chat={this.ChatRoom} />

                
            </Container>
            </Card>
            </Col>
            </Row>
        </Container>)
    }
}
export default withRouter(Main)