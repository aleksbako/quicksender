import React, { useState} from "react"
import {Navigate,useNavigate} from "react-router-dom"
import {Container,Col,Button, Navbar, NavDropdown, Nav, Form} from "react-bootstrap"

function TopNav(){
  const logout = () => {
    //Send to server tell that the token is no longer active
    document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    window.location.reload(true)
    return (<Navigate to={"/"}/>)
  }
    const navigate = useNavigate();
   const  [search, setSearch] = useState("")

   const FindContacts = () =>{
     
    console.log(search)
    navigate("/search", {replace: true, state: {search: search}})
    
}
    return (<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
    <Container>
 <Navbar.Brand href="/">QuickSender</Navbar.Brand>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
      <Nav className="me-auto">
        <Nav.Link href="#Blog">Blog</Nav.Link>
        <Nav.Link href="#Support">Support</Nav.Link>
        
          <Col>
          
        <Form.Control size="md" type="text" placeholder="Large text" onChange={e => e.keyCode == "13"? FindContacts() : setSearch(e.target.value)} />
        </Col>
        <Col>
        <Button variant="warning" onClick={FindContacts}>Search</Button>
        </Col>
        
      </Nav>
      <Nav>
        <Nav.Link href="#deets">About</Nav.Link>
        <Nav.Link eventKey={2} onClick={logout} >
          LogOut
        </Nav.Link>
      </Nav>
    </Navbar.Collapse>
    </Container>
  </Navbar>)
}export default TopNav