import React, { useEffect, useState } from "react"

import { useLocation, useParams, useNavigate } from "react-router";
import {Container, Row, Col, Button, FormText, Image} from "react-bootstrap"
import userimg from"./img/UserImage.jpg"

 function Profile(){
   const  [freq, setFreq] = useState(false)
   const [fstat, setFstat] = useState(false)
    let params = useParams();
    useEffect( () => {
        document.body.style.color = "white";
        document.body.style.background= "#1a1817"

        
          checkIfFriend()

        //Handle send friend request 
    })
 
    const checkIfFriend = async () => {
        //Make request to DB which checks if the users are friends or not . If they are friends then set unfriend button instead of send friend request.
        const token = document.cookie.split("=")[1]
        const response = await fetch("http://localhost:8080/restricted/post", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }).then(function(response) {
             
            return response.json();
          }).then(function(data) {
            return data
          });
         
      
          for(let i = 0 ; i < response.Contacts.length;i++){
            if(response.Contacts[i].contact_name == params.id){
              setFstat(true)
          }
          }
    }
    
    
    const unfriend = async () => {

        
        //Remove person from contact list, do the same for the other contact.
        const token = document.cookie.split("=")[1]
        const response = await fetch("http://localhost:8080/restricted/removeContact", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
            method: "POST",
            mode:"cors",
            body: new URLSearchParams("username="+params.id)
          }).then(function(response) {
             
            return response.text();
          }).then(function(data) {
            return data; // this will be a string
          });

        setFstat(false)
    }


    const FriendInvite = async () => {
        const token = document.cookie.split("=")[1]
        const response = await fetch("http://localhost:8080/restricted/inv", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
            method: "POST",
            mode:"cors",
            body: new URLSearchParams("username="+params.id)
          }).then(function(response) {
             
            return response.text();
          }).then(function(data) {
            return data; // this will be a string
          });
          console.log("testing")
        setFreq(true)
    }

    const CancelFriendInvite = async () =>{
        const token = document.cookie.split("=")[1]
        const response = await fetch("http://localhost:8080/restricted/uninv", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
            method: "POST",
            mode:"cors",
            body: new URLSearchParams("username="+params.id)
          }).then(function(response) {
             
            return response.text();
          }).then(function(data) {
            return data; // this will be a string
          });
        setFreq(false)
    }


    return (<Container>
        <Row>
            <Col>
            <h1>Profile of {params.id}</h1>
            </Col>
        </Row>
        <Row >
            <Col><Image src={userimg} className="image"/></Col>
          </Row>
          <Row className="mt-2">
            {!fstat ?
            <Col md={{span: 4}}> {!freq ?<Button onClick={() => FriendInvite()}>Send Friend Request</Button> : <Button onClick={() => CancelFriendInvite() }>Cancel Friend Request</Button>}</Col> :
            <Col>
            <Button onClick={() => unfriend()}>Unfriend</Button>
            </Col>
            }
        </Row>
    </Container>)
} export default Profile