import React, {Component} from "react"
import {Container,Row, Col, Image, Button, Form} from "react-bootstrap"
import TopNav from "../TopNav"
import userimg from "./img/UserImage.jpg"
import { BsChatDots } from "react-icons/bs";
import "./custom.css"
class Account extends Component{

    constructor(){
        super();
        this.testRestricted = this.testRestricted.bind(this)
    }
    componentDidMount(){
        document.body.style.color = "white";
        document.body.style.background= "black"
    }

   async testRestricted(){
       const token = document.cookie.split("=")[1]
        const response = await fetch("http://localhost:8080/restricted", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }).then(function(response) {
             
            return response.text();
          }).then(function(data) {
            console.log(data); // this will be a string
          });
    }
    render(){
        return(<Container fluid >
            <TopNav/>
            <Container>
            <Row>
                <h1>Account details</h1>
            </Row>
            <Row>
                <Col md={3}>
                <Image src={userimg} rounded className="usrimage" />
                </Col>
                
                <Col ><h2>SpiffyRaccon </h2>
                <Row>
                    <Col md={{span: 2}}>
                    <Button variant="secondary">Change profile Photo</Button>
                    </Col>
                </Row></Col>
            
            </Row>
            <Row>
                <h1>
                    Table of info
                
                </h1>
               <Container>
                   <Row md={6}>
                       <Col>
                       <Row>
                   <Col>
                       <h5>Name</h5>
                       </Col>
                       </Row>
                       <Row>
                       <Col>
                       <h5>Aleks</h5>
                       </Col>
                       </Row>
                       </Col>
                       <Col>
                       <Button onClick={this.testRestricted}>Edit</Button>
                       </Col>
                   </Row>
                   <Row md={6}>
                       <Col>
                       <Row>
                   <Col>
                       <h5>Email</h5>
                       </Col>
                       </Row>
                       <Row>
                       <Col>
                       <h5>******@outlook.com</h5>
                       </Col>
                       </Row>
                       </Col>
                       <Col>
                        <Button>Edit</Button>
                       </Col>
                   </Row>
                   <Row md={5}>
                   <Button >Change password</Button>
                   </Row>
               </Container>
            </Row>
            </Container>
           
           
        </Container>)
    }
}
export default Account