import React from "react"
import {Container, Navbar, NavDropdown, Nav} from "react-bootstrap"
function TopNav(){
    return (<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
    <Container>
 <Navbar.Brand href="/">QuickSender</Navbar.Brand>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
      <Nav className="me-auto">
        <Nav.Link href="#Blog">Blog</Nav.Link>
        <Nav.Link href="#Support">Support</Nav.Link>
      
      </Nav>
      <Nav>
        <Nav.Link href="#deets">About</Nav.Link>
        <Nav.Link eventKey={2} href="/login">
          Login
        </Nav.Link>
      </Nav>
    </Navbar.Collapse>
    </Container>
  </Navbar>)
}export default TopNav