import React from "react"
import {Carousel} from "react-bootstrap"
import Slide3 from "./img/Slide3.png"
import Slide2 from "./img/Slide2.jpg"
import Slide1 from "./img/Slide1.png"
import image from "./mainSlide.css"
function MainSlide(){
    return (<Carousel>
        <Carousel.Item interval={2000}>
          <img
            className="d-block w-100 image"
            src={Slide1}
            alt="First slide"
          />
          <Carousel.Caption>
            <h2 >Simplifying communication</h2>
            <h4>QuickSender strives to make your communication as smooth as possible.</h4>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={2000}>
          <img
            className="d-block w-100 image"
            src={Slide2}
            alt="Second slide"
          />
          <Carousel.Caption>
            <h2>Secure Communication</h2>
            <h4>Avoid anyone snooping on your messages by using QuickSender.</h4>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={2000}>
          <img
            className="d-block w-100 image "
            src={Slide3}
            alt="Third slide"
            
          />
          <Carousel.Caption>
            <h2>Improve Productivity</h2>
            <h4>Using QuickSender you can get that project up and running sooner than expected.</h4>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>)
}
export default MainSlide