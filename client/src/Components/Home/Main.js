import React, {Component} from "react"
import {Navigate} from "react-router-dom"
import {Container, Row, Col, Card} from "react-bootstrap"
import {BsLightningChargeFill, BsFillEyeSlashFill,BsLockFill,BsClipboardData} from 'react-icons/bs'
import TopNav from "../TopNav";
import MainSlide from "./MainSlide"
class Main extends Component{
   
    constructor(props){
       super(props)
        this.state = {
            loggedIn: false
        }
   } 
   componentDidMount() {
    document.body.style.color = "white";
    document.body.style.background= "#1a1817"
  }
   render(){
       if(document.cookie != ""){
          
           this.setState(()=> {
               return{loggedIn: true}})
           return <Navigate to={"/post"} />
       }
       return <Container fluid><TopNav /><Container >
           <Row><MainSlide/></Row>
           <h1>QuickSender</h1>
           <p>A small demo instant messaging website which requires a user to login and then it can communicate with the people that you have established a connection </p>
           <h2>Why use QuickSender</h2>
           <Row md="4">
           <Col>
           <Card style={{ width: '18rem',  height: '15rem' }}>
             <Card.Body className="customCard">
                <Row>
                <h2> <BsLightningChargeFill />  </h2>
             </Row>
                 <Card.Title>Speed</Card.Title>
                    <Card.Text>
                   Get in touch with your collegues and of friend very quickly through our fast messaging api.
                    </Card.Text>
  
  </Card.Body>
</Card>
</Col>
<Col>
<Card style={{ width: '18rem' , height: '15rem'}}>
             <Card.Body className="customCard">
                <Row>
                <h2>  <BsFillEyeSlashFill /> </h2>
                </Row>
                 <Card.Title >Privacy</Card.Title>
                
                    <Card.Text >
                   We keep all of your data safely encrypted so you can just focus on talking to people.
                    </Card.Text>
  
  </Card.Body>
</Card>
</Col>
<Col>
<Card style={{ width: '18rem' , height: '15rem'}}>
             <Card.Body className="customCard">
                <Row>
               <h2> <BsLockFill /> </h2>
                </Row>
                 <Card.Title >Security</Card.Title>
                 
                    <Card.Text >
                    Your account is safely protected by our authentication service. We protect your information, because you matter.
                    </Card.Text>
  
  </Card.Body>
</Card>
</Col>


<Col>
<Card style={{ width: '18rem' , height: '15rem'}}>
             <Card.Body className="customCard">
                <Row>
                <h2><BsClipboardData /> </h2>
                </Row>
                 <Card.Title >Productivity</Card.Title>
                 
                    <Card.Text >
                   Faster communication improves productivity, which means that project that you are working on will be done much quicker.
                    </Card.Text>
  
  </Card.Body>
</Card>
</Col>

</Row>
           </Container>
           </Container>
   }
}
export default Main