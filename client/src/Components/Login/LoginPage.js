import { Component } from "react";
import {Navigate,Link} from "react-router-dom"
import {Container, Row, Col, Form, Button,Card, CardGroup, Image} from 'react-bootstrap'
import Cookies from "universal-cookie"
import TopNav from "../TopNav";
import logo from "./img/test_logo.png"
import "./custom.css"

class LoginPage extends Component{
    
    constructor(props){
        super(props)
        this.state ={
            login: "",
            password: "",
            isLoggedIn: false,
            WrongInfo: false
        }
        this.UpdateL = this.UpdateL.bind(this)
        this.UpdateP = this.UpdateP.bind(this)
        this.Login = this.Login.bind(this)
    }
    componentDidMount() {
        document.body.style.color = "white";
        document.body.style.backgroundColor = "#1a1817";
      }

    UpdateL(event){
        this.setState(() => {
            return {login: event.target.value}
        })
    
    }
    UpdateP(event){
        this.setState(() => {
            return {password: event.target.value}
        })
     
    }

    async Login(){
 
       const result = await fetch ("http://localhost:8080/login", {
            method: "POST",
            mode:"cors",
            body: new URLSearchParams("username="+this.state.login + "&" + "password=" + this.state.password)
       })
         .then((response) => response.json())
         .then((result) => {
            return result
         });
       
       if(result.token != undefined && result.token != ""){
           this.SaveJWTinCookie(result)
           
           this.setState(()=>{
               return{
               isLoggedIn: true,
               WrongInfo: false
           }
           })
       }
       else{
        this.setState(()=>{
            return{
            WrongInfo: true
        }
        })
       }
  
    }
    
    SaveJWTinCookie(response){
        const cookies = new Cookies();
        cookies.set("token", response.token, {
            path: '/',
            httpOnly: false,
            maxAge: 604800
        });
        console.log(cookies.get("token"));
        
    }
    render(){
        //make it so that if wrong input give error message
        const val = ""
        if(this.state.isLoggedIn){
            return <Navigate to={"/post"}/>
        }
        
      
        else{

        
        return <Container fluid className="bg-#1a1817"><TopNav/> <Container className="LoginContainer" ><Card className="bg-warning outer mt-5"> 
       
        
        <CardGroup >
          
            <Col >
           
      <Card className=" bg-warning">
      
            <Image className="logo" src={logo} />
      </Card>
      </Col>
    <Col>
    <Card >
            
            <Card className="cardcolor LoginCard">
              <Row className="mt-5 text-center">
              <h2>Hello!</h2>
              </Row>
                <Row className="text-center">
                    <Col className="text-center">
                    <p>Dont have an account?</p> <p>Create one <Link to={"/register"}> here </Link></p>
                    </Col>
                    
                </Row>
              <Row className="loginForm">
                 
                  <Form >
                  
                  <Row >
                  <label htmlFor="floatingInputCustom">Login</label>
              <Form.Control
              
              id="Username"
              type="username"
              placeholder="username"
              onChange={this.UpdateL}
            /></Row>
      <Row >
         
      <label htmlFor="floatingInputCustom">Password</label>
      <Form.Control
              id="Passoword"
              type="password"
              placeholder="password"
              onChange={this.UpdateP}
            />
            <Col md={{offset :8 }}>
                <Link to={"/"}><p>Forgot password</p></Link>
            </Col>
           
            </Row>
           
            <Row >
            
             <Button  variant="warning" type="button" onClick={this.Login}>
          Login
        </Button>
       
      
        </Row>
        
            </Form>
        
            </Row>
      
            </Card>
            
            </Card>
           
    </Col>
    
    </CardGroup>
    
      </Card>
      </Container>
      </Container>
    }
}
}
export default LoginPage