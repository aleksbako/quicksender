import React, {Component} from "react"
import { Card ,CardGroup,Container, Row, Col, Image, Form, Button} from "react-bootstrap"
import Cookies from "universal-cookie"
import {Navigate} from "react-router"
import withRouter from "../../withRouter"

class Register extends Component {
    constructor(props){
        super(props)
        this.state = {

            username: "",
            email: "",
            password: "",
            created: false
        }
        this.updateName = this.updateName.bind(this)
        this.updateEmail = this.updateEmail.bind(this)
        this.updatePassword = this.updatePassword.bind(this)
        this.createUser = this.createUser.bind(this)
        this.login = this.login.bind(this)
    }

    async createUser(){
        const result = await fetch ("http://localhost:8080/register", {
            method: "POST",
            mode:"cors",
            body: new URLSearchParams("username="+this.state.username + "&" + "password=" + this.state.password + "&" + "email="+ this.state.email)
       })
         .then((response) => response.text())
         .then((result) => {
            return result
         });
         if(result == "user created"){
             this.setState(() => {return {created: true}})
            this.login()
         }
    }

    async login(){
        const result = await fetch ("http://localhost:8080/login", {
            method: "POST",
            mode:"cors",
            body: new URLSearchParams("username="+this.state.username + "&" + "password=" + this.state.password)
       })
         .then((response) => response.json())
         .then((result) => {
            return result
         });

         const cookies = new Cookies();
         cookies.set("token", result.token, {
             path: '/',
             httpOnly: false,
             maxAge: 604800
         });
        
    }

    updateName(event){
        this.setState(()=> {return {username: event.target.value}})
    }
    updateEmail(event){
        this.setState(()=> {return {email: event.target.value}})
    }
    updatePassword(event){
        this.setState(()=> {return {password: event.target.value}})
    }


    render(){
        if(this.state.created){
            return <Navigate to={"/post"}/>
        }else {
        return (<Container className="LoginContainer">
            <Row className="text-dark">
         
            <Col md={{span: 5, offset: 3}}>
            <Card className="bg-dark text-light">
                <h1>
                    Account creation
                </h1>
            <Card.Body>
               <Form>

               <Form.Group className="mb-3" controlId="usernameForm">
    <Form.Label>Username</Form.Label>
    <Form.Control type="username" placeholder="Enter username" onChange={e => this.updateName(e)} />

  </Form.Group>

               <Form.Group className="mb-3" controlId="emailForm">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="Enter email" onChange={e => this.updateEmail(e)} />
    <Form.Text className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text>
  </Form.Group>

  <Form.Group className="mb-3" controlId="passwordForm">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" onChange={e => this.updatePassword(e)}/>
  </Form.Group>



               </Form>
               <Button variant="warning" type="submit" onClick={this.createUser}>
    Submit
  </Button>
            </Card.Body>
            </Card>
            </Col>
            </Row>
            </Container>)
        
    }

    }

}export default withRouter(Register)

