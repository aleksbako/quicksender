# QuickSender

## How to run

 * Open 2 terminals:
 * In terminal one run "cd /client"
 * Run "npm start"
 * In terminal 2 run "cd /server"
 * Run "go build"
 * Then "./quicksender"


## About QuickSender

QuickSender is an instant messaging application made to imporve my abilities with websockets, and also to develope a better understanding behind instant messagning applications.

Main page before Logging in:
<img src="./resources/Pre-login_home_screenshot.png">

Login Page: 
<img src="./resources/login_screenshot.jpg">

Main page after Logging in:
<img src="./resources/post-login_home_screenshot.jpg">

Search result page, after searching for contacts:
<img src="./resources/search_screenshot.jpg">

Messaging Page:
<img src="./resources/chat_screenshot.PNG">





