package models

import "github.com/golang-jwt/jwt"

type JwtCustomClaims struct {
	Name  string `json:"name"`
	Admin bool   `json:"admin"`
	jwt.StandardClaims
}

type HomeData struct {
	Contacts []ContactInfo `json:contacts`
	Invites  []string      `json:"invites"`
}

type ContactInfo struct {
	Name   string `json:"contact_name"`
	ChatId string `json:"chat_id"`
}

type SearchMsg struct {
	Contacts []string `json:"contacts"`
}

type Message struct {
	From string `json:"from"`
	Data string `json:"data"`
}

type ChatContacts struct {
	Contacts []ContactInfo `json:contacts`
}
