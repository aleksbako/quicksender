package routes

import (
	"fmt"
	"net/http"
	"quicksender/database"
	"quicksender/models"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/thanhpk/randstr"
)

func AddContact(c echo.Context) error {
	username := c.FormValue("username")
	user := c.Get("user").(*jwt.Token)

	claims := user.Claims.(*models.JwtCustomClaims)
	name := claims.Name
	fmt.Println(name)
	fmt.Println(username)
	database.DbSentUninvites(username, name)

	database.DbUninvites(username, name)

	database.AddDbContact(name, username)
	database.AddDbContact(username, name)

	AssignChatId(name, username)

	return c.String(http.StatusOK, "")

}

func RemoveContact(c echo.Context) error {
	username := c.FormValue("username")
	user := c.Get("user").(*jwt.Token)

	claims := user.Claims.(*models.JwtCustomClaims)
	name := claims.Name

	database.RemoveDbContact(name, username)
	database.RemoveDbContact(username, name)

	return c.String(http.StatusOK, "")
}

func ContactInv(c echo.Context) error {
	username := c.FormValue("username")
	user := c.Get("user").(*jwt.Token)

	claims := user.Claims.(*models.JwtCustomClaims)
	name := claims.Name

	database.DbSentInvites(name, username)
	database.DbInvites(name, username)

	return c.String(http.StatusOK, "")
}
func ContactUnInv(c echo.Context) error {
	username := c.FormValue("username")
	user := c.Get("user").(*jwt.Token)

	claims := user.Claims.(*models.JwtCustomClaims)
	name := claims.Name
	database.DbSentUninvites(username, name)
	database.DbUninvites(username, name)

	return c.String(http.StatusOK, "")
}

func AssignChatId(user, contact string) {
	if !database.CheckIfIdExist(user, contact) {
		id := randstr.Hex(16)
		for database.CheckIfIDExists(id) {
			id = randstr.Hex(16)
		}

		database.CreateChatID(id, user+","+contact)
	}
}
