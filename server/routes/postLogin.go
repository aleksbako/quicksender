package routes

import (
	"fmt"
	"net/http"
	"quicksender/database"
	"quicksender/models"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

func HomePostLogin(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)

	claims := user.Claims.(*models.JwtCustomClaims)
	name := claims.Name

	contacts := database.GetDbGetContacts(name)

	contactsobj := []models.ContactInfo{}
	for _, contact := range contacts {
		fmt.Println(contact)
		id := database.GetDBChatId(name, contact)
		contactsobj = append(contactsobj, models.ContactInfo{contact, id})
	}
	invites := database.GetDbGetInvites(name)

	return c.JSON(http.StatusOK, models.HomeData{Contacts: contactsobj, Invites: invites})

}

func GetSearch(c echo.Context) error {
	search := c.FormValue("search")

	contacts := database.GetDBSearch(search)
	if contacts != nil {
		return c.JSON(http.StatusOK, models.SearchMsg{Contacts: contacts})
	} else {
		return c.String(http.StatusOK, search)
	}
}

func GetUserName(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)

	claims := user.Claims.(*models.JwtCustomClaims)
	name := claims.Name
	return c.String(http.StatusOK, name)

}
