package routes

import (
	"fmt"
	"net/http"
	"quicksender/chat"
	"quicksender/database"
	"quicksender/models"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

var hubs = make(map[string]*chat.Hub)

func ConnectToWs(c echo.Context) error {
	if token, err := c.Cookie("token"); err == nil {

		fmt.Println(c.Request().URL.Path)
		id := strings.TrimPrefix(c.Request().URL.Path, "/ws/")
		if room, ok := hubs[id]; ok {

			return chat.ServeWs(token.Value, room, c)
		} else {

			hub := chat.NewHub(id)
			go hub.Run()
			hubs[id] = hub
			return chat.ServeWs(token.Value, hub, c)
		}

	} else {
		return ServeHome(c)
	}

}

func GetChatContacts(c echo.Context) error {

	user := c.Get("user").(*jwt.Token)

	claims := user.Claims.(*models.JwtCustomClaims)
	name := claims.Name
	contacts := database.GetDbGetContacts(name)

	contactsobj := []models.ContactInfo{}
	for _, contact := range contacts {
		fmt.Println(contact)
		id := database.GetDBChatId(name, contact)
		contactsobj = append(contactsobj, models.ContactInfo{contact, id})
	}

	return c.JSON(http.StatusOK, models.ChatContacts{Contacts: contactsobj})
}
