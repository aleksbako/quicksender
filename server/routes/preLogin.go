package routes

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"log"
	"net/http"
	"quicksender/database"
	"quicksender/models"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/thanhpk/randstr"
)

func ServeHome(c echo.Context) error {

	log.Println(c.Request().URL)
	if c.Request().URL.Path != "/" {
		return errors.New("error")

	}

	fmt.Printf("%s\n", "testing")
	return nil
}

func Login(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")
	fmt.Println(password)

	salt, p := database.GetDbPassword(username)

	if p != sha256.Sum256([]byte(salt+password)) {
		return c.JSON(http.StatusOK, "")
	} else {
		// Throws unauthorized error
		//Do account check
		// Set custom claims
		claims := &models.JwtCustomClaims{
			username,
			true,
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			},
		}

		// Create token with claims
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte("secret"))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, echo.Map{
			"token": t,
		})
	}
}

func Register(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")
	email := c.FormValue("email")
	salt := randstr.Hex(16)
	hashedpassword := sha256.Sum256([]byte(salt + password))

	exists := database.CheckIfUserExist(username)

	if exists {
		return c.String(http.StatusOK, "user exists")
	} else {
		database.CreateUser(username, salt, email, hashedpassword)
		return c.String(http.StatusOK, "user created")
	}

}
