package database

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
)

func Db() *sql.DB {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	return db

}

// Searches for a pattern of a username within the database and returns the results list.
func GetDBSearch(str string) []string {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `SELECT username FROM user where username LIKE $1`

	rows, er := db.Query(query, "%"+str+"%")

	if er != nil {
		return nil
	}
	var usernames []string
	var user string
	defer rows.Close()
	for rows.Next() {

		er := rows.Scan(&user)
		if er != nil {
			panic(er)
		}
		usernames = append(usernames, user)
	}

	return usernames
}
