package database

import (
	"database/sql"
	"encoding/json"
	"fmt"
)

//Gets the user's salt and hashed password from the database
func GetDbPassword(name string) (string, [32]byte) {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `Select salt , password from user where username=$1`

	rows, er := db.Query(query, name)
	if er != nil {
		fmt.Errorf("error at query")
	}

	var password []byte
	var salt string
	defer rows.Close()
	for rows.Next() {

		er := rows.Scan(&salt, &password)
		if er != nil {
			panic(er)
		}

	}
	var Unmarshpassword [32]byte
	json.Unmarshal(password, &Unmarshpassword)
	return salt, Unmarshpassword
}

//Checks database if there exists a user with the same username.
func CheckIfUserExist(username string) bool {

	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `Select username from user where username=$1`

	rows, er := db.Query(query, username)
	if er != nil {
		if er == sql.ErrNoRows {
			fmt.Println("testing")
			return false
		}
		fmt.Errorf("error at query")
	}

	name := ""
	defer rows.Close()
	for rows.Next() {

		er := rows.Scan(&name)
		if er != nil {
			panic(er)
		}

	}

	if name == "" {
		return false
	}
	return true

}

//Creates a new user entry within the database.
func CreateUser(username, salt, email string, password [32]byte) {

	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()
	pass, _ := json.Marshal(password)
	query := `INSERT INTO user (username, password ,salt, email) VALUES ($1, $2, $3,$4)`

	_, er := db.Exec(query, username, pass, salt, email)
	fmt.Println("testing if error occoured")
	if er != nil {
		fmt.Println("testing if error occoured in er != nil")
		fmt.Println(er)
		fmt.Errorf("error at query")
	}

}
