package database

import (
	"database/sql"
	"fmt"
	"strings"
)

func GetDbGetContacts(name string) []string {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}

	defer db.Close()
	query := `Select contact from user where username=$1`

	rows, er := db.Query(query, name)
	fmt.Println("Testing rows stuff ")
	if er != nil {
		return []string{}
	}
	var contacts string
	defer rows.Close()
	for rows.Next() {

		er = rows.Scan(&contacts)

	}
	if len(contacts) == 0 {
		return []string{}
	}
	contactlist := strings.Split(contacts, ",")
	return contactlist
}

func AddDbContact(username, contact string) {

	contactlist := GetDbGetContacts(username)

	contactlist = append(contactlist, contact)

	contacts := ""

	if len(contactlist) > 0 {
		for _, val := range contactlist {

			contacts += val + ","
		}

		contacts = contacts[:len(contacts)-1]
	}
	fmt.Println("add user to db 2")
	UpdateDbContact(username, contacts)

}

func RemoveDbContact(username, contact string) {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}

	contactlist := GetDbGetContacts(username)
	db.Close()
	fmt.Println(contactlist)
	for i, val := range contactlist {
		if val == contact {

			contactlist[i] = contactlist[len(contactlist)-1]
			contactlist[len(contactlist)-1] = ""
			contactlist = contactlist[:len(contactlist)-1]
			break
		}
	}

	contacts := ""
	if len(contactlist) > 0 {
		for _, val := range contactlist {
			contacts += val + ","
		}

		contacts = contact[:len(contacts)-1]
	}

	UpdateDbContact(username, contacts)

}

func UpdateDbContact(user, contactlist string) {
	db, _ := sql.Open("sqlite3", "./database/quicksender.db")

	defer db.Close()

	query := `UPDATE user
	SET contact = $1
   WHERE username = $2`
	_, er := db.Exec(query, contactlist, user)
	if er != nil {
		fmt.Errorf("error occoursed updating db database")
	}
}
