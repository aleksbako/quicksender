package database

import (
	"database/sql"
	"fmt"
	"strings"
)

//Gets all the invites that user has recieved.
func GetDbGetInvites(name string) []string {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `Select invites from user where username=$1`

	rows, er := db.Query(query, name)

	if er != nil {
		return nil
	}
	var invites string
	defer rows.Close()
	for rows.Next() {

		er = rows.Scan(&invites)

	}
	if len(invites) == 0 {
		return []string{}
	}
	invitelist := strings.Split(invites, ",")
	return invitelist
}

//Removes user from contacts(username) invites entry.
func DbUninvites(user, username string) {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	inviteslist := GetDbGetInvites(username)
	for i, val := range inviteslist {
		if val == user {
			inviteslist[i] = inviteslist[len(inviteslist)-1]
			inviteslist[len(inviteslist)-1] = ""
			inviteslist = inviteslist[:len(inviteslist)-1]
			break
		}
	}

	invites := ""
	if len(inviteslist) > 0 {
		for _, val := range inviteslist {
			invites += val + ","
		}
		invites = invites[:len(invites)-1]
	}

	UpdateInvites(username, invites)

}

//Adds user to contacts invite entry.
func DbInvites(user, username string) {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	inviteslist := GetDbGetInvites(username)

	inviteslist = append(inviteslist, user)

	invites := ""

	for _, val := range inviteslist {
		invites += val + ","
	}

	invites = invites[:len(invites)-1]

	UpdateInvites(username, invites)

}

//Updates invites entries for user.
func UpdateInvites(user, invites string) {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()
	query := `UPDATE user
	SET invites = $1
   WHERE username = $2`

	_, er := db.Exec(query, invites, user)

	if er != nil {

		fmt.Errorf("error occoursed updating db database")
	}

}

//Get the list of users that 'name' has sent contact invitations to.
func GetDbGetSentInvites(name string) []string {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `Select sent_invites from user where username=$1`

	rows, er := db.Query(query, name)

	if er != nil {
		return nil
	}
	var invites string
	defer rows.Close()
	for rows.Next() {

		er = rows.Scan(&invites)

	}
	if len(invites) == 0 {
		return []string{}
	}
	invitelist := strings.Split(invites, ",")
	return invitelist
}

//Removes the 'username' from user's sent invites table.
func DbSentUninvites(user, username string) {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	inviteslist := GetDbGetSentInvites(user)
	for i, val := range inviteslist {
		if val == username {
			inviteslist[i] = inviteslist[len(inviteslist)-1]
			inviteslist[len(inviteslist)-1] = ""
			inviteslist = inviteslist[:len(inviteslist)-1]
			break
		}
	}

	invites := ""
	if len(inviteslist) > 0 {
		for _, val := range inviteslist {
			invites += val + ","
		}
		invites = invites[:len(invites)-1]
	}
	UpdateSentInvites(user, invites)

}

//Adds contacts username to user's sent invites table entry.
func DbSentInvites(user, username string) {
	db := Db()
	defer db.Close()

	inviteslist := GetDbGetSentInvites(user)

	inviteslist = append(inviteslist, username)

	invites := ""
	if len(inviteslist) > 0 {
		for _, val := range inviteslist {
			invites += val + ","
		}

		invites = invites[:len(invites)-1]
	}
	UpdateSentInvites(user, invites)

}

//Updates the sent invites table for user with new list of invites.
func UpdateSentInvites(user, invites string) {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `UPDATE user
	SET sent_invites = $1
   WHERE username = $2`

	_, er := db.Exec(query, invites, user)

	if er != nil {

		fmt.Errorf("error occoursed updating db database")
	}

}
