package database

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
)

//Checks if the there exists already a chatroom entry for the 2 users within the chat table.
func CheckIfIdExist(user, contact string) bool {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `SELECT plist
	FROM chat where plist LIKE $1 and plist LIKE $2`
	rows, er := db.Query(query, "%"+user+"%", "%"+contact+"%")
	if er != nil {
		if er != sql.ErrNoRows {
			// a real error happened! you should change your function return
			// to "(bool, error)" and return "false, err" here
			log.Print(er)
		}
		return false
	}
	for rows.Next() {
		var plist string
		er = rows.Scan(&plist)
		if plist == user+","+contact || plist == contact+","+user {
			return true
		}

	}

	return false
}

//Creats a database table record with id and the contacts.
func CreateChatID(id, plist string) {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}

	fmt.Println(plist)
	fmt.Println(id)
	query := `INSERT INTO chat (id, plist) VALUES ($1, $2)`
	_, er := db.Exec(query, id, plist)
	fmt.Println("post insert check for errors ")
	fmt.Println(er)

	if er != nil {
		fmt.Errorf("Error on chat insertion")
	}
	db.Close()
}

//Updates the data column for record with primary key = id.
func UpdateChatData(id string, data []byte) {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `UPDATE chat
	SET data = $1
	WHERE id = $2`
	_, er := db.Exec(query, data, id)
	if er != nil {

		fmt.Errorf("error occoured updating chat data")
	}
}

//Checks if the chat id already exists, then return true, else false.
func CheckIfIDExists(id string) bool {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `SELECT EXISTS(Select id
	FROM chat where id = $1)`
	rows, er := db.Query(query, id)
	if er != nil {
		fmt.Println(er)
		if er != sql.ErrNoRows {

			log.Print(er)

		}

		return false
	}
	for rows.Next() {
		var str string

		rows.Scan(&str)

		if str == "0" {
			rows.Close()
			return false
		}
	}

	return true
}

//Gets the chat id between 2 users such that they can go to their chat room.
func GetDBChatId(user, contact string) string {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `SELECT id, plist
	FROM chat where plist LIKE $1 and plist LIKE $2`
	rows, er := db.Query(query, "%"+user+"%", "%"+contact+"%")
	if er != nil {

		log.Print(er)

	}
	defer rows.Close()
	for rows.Next() {
		var plist string
		var id string

		er = rows.Scan(&id, &plist)

		if plist == user+","+contact || plist == contact+","+user {
			return id
		}

	}

	return ""
}

//Get the previous chat data so that the user once rejoined the chat has all the logs.
func GetDBChatData(id string) [][]byte {
	db, err := sql.Open("sqlite3", "./database/quicksender.db")
	if err != nil {
		fmt.Errorf("error occoursed opening database")
	}
	defer db.Close()

	query := `SELECT data
	FROM chat where id = $1`
	rows, er := db.Query(query, id)
	if er != nil {

		log.Print(er)

	}
	var blob []byte
	defer rows.Close()
	for rows.Next() {

		er = rows.Scan(&blob)

	}
	var oldmsg [][]byte
	json.Unmarshal(blob, &oldmsg)

	return oldmsg
}
