package chat

import (
	"encoding/json"
	"quicksender/database"
)

type Hub struct {
	id         string
	clients    map[*Client]bool
	oldmsg     [][]byte
	broadcast  chan []byte
	register   chan *Client
	unregister chan *Client
}

func NewHub(chat_id string) *Hub {
	return &Hub{
		id:         chat_id,
		broadcast:  make(chan []byte),
		oldmsg:     []([]byte){},
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

func (h *Hub) Run() {

	for {
		select {

		case client := <-h.register:
			if len(h.clients) == 0 {

				h.FetchOldMsg()
			}
			h.clients[client] = true

			for _, msg := range h.oldmsg {
				h.ProvideOldData(client, msg)
			}
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)

				if len(h.clients) == 0 {
					blob, er := json.Marshal(h.oldmsg)
					if er != nil {
						break
					}
					database.UpdateChatData(h.id, blob)
				}
			}

		case message := <-h.broadcast:

			h.oldmsg = append(h.oldmsg, message)

			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}

func (h *Hub) ProvideOldData(client *Client, msg []byte) {
	for c, _ := range h.clients {
		if client == c {
			select {
			case client.send <- msg:
			default:
				close(client.send)
				delete(h.clients, client)
			}
		}
	}
}

func (h *Hub) FetchOldMsg() {
	h.oldmsg = database.GetDBChatData(h.id)
}
