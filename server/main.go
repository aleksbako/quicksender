package main

import (
	"flag"
	"net/http"
	"quicksender/models"
	routes "quicksender/routes"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {

	e := echo.New()
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	flag.Parse()

	e.GET("/", routes.ServeHome)

	// CORS restricted with a custom function to allow origins
	// and with the GET, PUT, POST or DELETE methods allowed.
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:3000"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	// Login route
	e.POST("/login", routes.Login)
	e.POST("/register", routes.Register)
	e.GET("/ws/:id", routes.ConnectToWs)

	// Restricted group
	r := e.Group("/restricted")

	// Configure middleware with the custom claims type
	config := middleware.JWTConfig{
		Claims:     &models.JwtCustomClaims{},
		SigningKey: []byte("secret"),
	}

	r.Use(middleware.JWTWithConfig(config))

	r.GET("/post", routes.HomePostLogin)
	r.GET("/usrn", routes.GetUserName)
	r.GET("/chatcontact", routes.GetChatContacts)
	r.POST("/search", routes.GetSearch)
	r.POST("/addContact", routes.AddContact)
	r.POST("/removeContact", routes.RemoveContact)
	r.POST("/inv", routes.ContactInv)
	r.POST("/uninv", routes.ContactUnInv)

	e.Logger.Fatal(e.Start(":8080"))

}
